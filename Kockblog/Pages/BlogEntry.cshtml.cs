﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kockblog.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Kockblog.Pages
{


    public class BlogEntryModel : PageModel
    {
        private readonly BlogContext context;
        public Blog blog;

        public BlogEntryModel(BlogContext context)
        {
            this.context = context;
        }

        public async Task<IActionResult> OnGet(Guid id)
        {
            var guzu = context.Links.ToList();
            if (id != null)
            {
                string guidstring = id.ToString();
                Link returnlink = await context.Links.FirstOrDefaultAsync(x => x.Guid == guidstring);
                if (returnlink != null)
                {
                    if (returnlink.TimeOfExpire > DateTime.UtcNow)
                    {
                        this.blog = await context.Blogs.FirstOrDefaultAsync(x => x.Id == returnlink.BlogID);
                        if (this.blog != null)
                        {
                            return Page();
                        }
                    }
                    else
                    {
                        //context.Links.Remove(returnlink);
                        string br = "";
                    }

                }
            }
            return Redirect("/");
        }
    }
}