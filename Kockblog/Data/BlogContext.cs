﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Kockblog.Data
{
    public class BlogContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Link> Links { get; set; }


        public BlogContext(DbContextOptions<BlogContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Blog>(entity =>
            {
                entity.Property(e => e.Content);
                entity.HasKey(e => e.Id);
                entity.Ignore(e => e.Tags);
                entity.Property(e => e.Title);
                entity.Property(e => e.DateOfCreation);
                entity.Property(e => e.DateOfLastEdit);
            });

            modelBuilder.Entity<Link>(entity =>
            {
                entity.HasKey(e => e.id);
                entity.Property(e => e.Guid);
                entity.Property(e => e.BlogID);
                entity.Property(e => e.TimeOfExpire);
            });
        }
    }
}
