﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kockblog.Data
{
    public class Link
    {
        public int id { get; set; }
        public string Guid { get; set; }
        public  int BlogID { get; set; }
        public DateTime TimeOfExpire { get; set; }

    }
}
